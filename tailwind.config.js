/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    `./src/pages/**/*.{js,jsx,ts,tsx}`,
    `./src/components/**/*.{js,jsx,ts,tsx}`,
  ],
  theme: {
    extend: {
      colors: {
        main: {
          blue: {
            500: '#0D47A1',
            700: '#062873',
          },
        }
      },
      fontFamily: {
        poppins: 'poppins',
      }
    },
  },
  plugins: [],
}
