import * as React from "react";
import { Link, type HeadFC, type PageProps } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
import DemoButton from "../components/buttons/demo";
import ConsultationButton from "../components/buttons/consultation";
import Header from "../components/header";
import Footer from "../components/footer";

const features: {
  id: number;
  imgSrc: string;
  imgAlt: string;
  title: string;
  description: string;
}[] = [
  {
    id: 1,
    imgSrc: "/images/f1.png",
    imgAlt: "Surat Masuk",
    title: "Surat Masuk",
    description:
      "Pencatatan, penyimpanan, pemantauan surat masuk secara sistematis. Dilengkapi atribut surat, upload dokumen dan tujuan bersama.",
  },
  {
    id: 2,
    imgSrc: "/images/f2.png",
    imgAlt: "Disposisi",
    title: "Disposisi",
    description:
      "Distribusi surat dan tindak lanjut ke pihak terkait dapat dilakukan langsung melalui aplikasi SIPAS.",
  },
  {
    id: 3,
    imgSrc: "/images/f3.png",
    imgAlt: "Ekspedisi",
    title: "Ekspedisi",
    description:
      "Cek status distribusi dan respon surat melalui aplikasi SIPAS.",
  },
  {
    id: 4,
    imgSrc: "/images/f4.png",
    imgAlt: "Surat Keluar",
    title: "Surat Keluar",
    description:
      "Pencatatan surat keluar dengan detail atribut surat, upload dokumen dan tujuan penerima.",
  },
  {
    id: 5,
    imgSrc: "/images/f5.png",
    imgAlt: "Template Surat",
    title: "Template Surat",
    description:
      "Buat surat langsung dengan aplikasi dengan template dan lakukan kustomisasi sesuai kebutuhan.",
  },
  {
    id: 6,
    imgSrc: "/images/f6.png",
    imgAlt: "Korespondensi Surat",
    title: "Korespondensi Surat",
    description:
      "Ketahui hubungan korespondensi pada surat masuk dan surat keluar.",
  },
  {
    id: 7,
    imgSrc: "/images/f7.png",
    imgAlt: "Surat Internal",
    title: "Surat Internal",
    description:
      "Distribusi surat di lingkungan internal langsung dari aplikasi SIPAS.",
  },
  {
    id: 8,
    imgSrc: "/images/f8.png",
    imgAlt: "Asistensi Monitoring",
    title: "Asistensi Monitoring",
    description:
      "Monitoring surat masuk hingga disposisi lebih mudah dengan asistensi dari oranga lain yang ditugaskan.",
  },
  {
    id: 9,
    imgSrc: "/images/f9.png",
    imgAlt: "Penomoran Otomatis",
    title: "Penomoran Otomatis",
    description:
      "Penomoran surat secara otomatis dengan format yang fleksibel.",
  },
];

const IndexPage: React.FC<PageProps> = () => {
  return (
    <>
      <Header />
      <div className="flex min-h-screen w-full flex-col font-poppins">
        <main className="bg-main-blue-500 text-white md:h-[110vh]">
          <section className="relative z-10 flex flex-col items-center justify-center gap-8 pb-20 pt-40 text-center">
            <h1 className="text-4xl font-black">
              Kelola Surat dan Disposisi <br /> Lebih Mudah, Cepat, dan Lebih
              Baik.
            </h1>
            <p className="text-slate-200">
              Lebih dari 20 lembaga ternama di Indonesia telah menggunakan SIPAS{" "}
              <br />
              dalam proses surat menyurat.
            </p>
          </section>
          <div className="relative z-10 flex justify-center gap-2">
            <ConsultationButton />
            <DemoButton />
          </div>

          <div className="relative -z-0 mt-8 flex items-end justify-center">
            <StaticImage
              src="../images/background.webp"
              alt="bg"
              className="absolute"
            />
            <StaticImage
              src="../images/homepage-sipas1-2048x1311.webp"
              alt="app"
              className="relative -mb-20 md:-mb-60 w-3/4"
            />
          </div>
        </main>

        <section className="mx-auto mt-32 md:mt-72 max-w-screen-lg bg-white" id="why">
          <div className="grid grid-cols-1 grid-rows-3 gap-x-8 md:grid-cols-3 md:grid-rows-1">
            {[
              {
                id: 1,
                imgSrc: "/images/image-49.png",
                imgAlt: "efective",
                title: "Kelola Surat dengan Efektif",
                description:
                  "Dilengkapi fitur unggulan yang membuat sistem persuratan di perusahaan Anda terkelola dengan baik.",
              },
              {
                id: 2,
                imgSrc: "/images/image-48.png",
                imgAlt: "disposition",
                title: "Disposisi Lebih Mudah dan Cepat",
                description:
                  "Disposisi bisa direspon lebih mudah dan cepat melalui smartphone",
              },
              {
                id: 3,
                imgSrc: "/images/image-47.png",
                imgAlt: "feature",
                title: "Fitur Lengkap",
                description:
                  "Aplikasi SIPAS menyediakan solusi praktis dan mudah dalam satu aplikasi.",
              },
            ].map((data) => (
              <div
                className="flex flex-col items-center  gap-4 md:flex-row md:items-start"
                key={data.id}
              >
                <div className="h-10 w-10">
                  <img
                    src={data.imgSrc}
                    alt={data.imgAlt}
                    className="object-contain"
                  />
                </div>
                <div className="flex w-fit flex-col items-center gap-2 md:items-start">
                  <span className="whitespace-nowrap font-bold">
                    {data.title}
                  </span>
                  <p className="w-3/4 text-center text-base font-light text-slate-600 sm:w-1/2 md:w-full md:text-left">
                    {data.description}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </section>

        <section className="mx-auto my-10 max-w-screen-lg bg-white" id="feature">
          <div className="mx-auto mb-8 flex w-3/4 flex-col items-center justify-center gap-1 md:mx-0 md:w-full">
            <span className="font-semibold text-main-blue-500">FITUR</span>
            <h2 className="text-center text-3xl font-black md:text-left">
              Permudah Kelola Surat dengan Fitur Terlengkap
            </h2>
            <p className="text-center md:text-left">
              Tingkatkan efektivitas pengelolaan surat dengan fitur-fitur
              unggulan aplikasi SIPAS.
            </p>
          </div>
          <div className="grid grid-flow-row grid-cols-1 gap-8 md:grid-cols-3 md:grid-rows-3">
            {features.map((data) => (
              <div
                className="flex flex-col items-center gap-4 md:flex-row md:items-start"
                key={data.id}
              >
                <div className="!h-10 !w-10">
                  <img
                    src={data.imgSrc}
                    alt={data.imgAlt}
                    className="h-10 w-10 object-contain"
                  />
                </div>

                <div className="flex w-3/4 flex-col items-center gap-2 md:w-fit md:items-start">
                  <h2 className="whitespace-nowrap font-bold">{data.title}</h2>
                  <p className="text-center text-base font-light text-slate-600 md:text-left">
                    {data.description}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </section>

        <section className="bg-main-blue-500" id="mobile">
          <div className="mx-auto my-10 flex max-w-screen-lg flex-col gap-8 md:flex-row">
            <div className="flex flex-col justify-center px-5 text-white md:px-0">
              <div className="flex flex-col gap-4">
                <span className="font-semibold">SIPAS MOBILE</span>
                <h2 className="text-3xl font-black">
                  Hilangkan batasan anda, akses surat kapan dan dimana saja
                </h2>
                <p>
                  Respon disposisi lebih mudah dan fleksibel melalui smartphone.
                  Sesuai untuk Anda yang punya mobilitas tinggi.
                </p>
              </div>

              <div className="mt-6 grid w-fit grid-cols-2 grid-rows-3 gap-6">
                {[
                  "Disposisi",
                  "Asistensi monitoring",
                  "Surat masuk",
                  "Realtime notifikasi",
                  "Koreksi surat",
                ].map((text) => (
                  <div className="flex gap-2" key={text}>
                    <div className="h-6 w-6 rounded-full bg-green-500 p-1">
                      <StaticImage
                        src="../images/check.svg"
                        alt="check"
                        className="h-4 w-4"
                      />
                    </div>
                    <span className="font-bold">{text}</span>
                  </div>
                ))}
              </div>

              <div className="mt-8 flex justify-center gap-4 md:justify-start">
                <a href="#">
                  <img
                    src="https://www.sipas.id/wp-content/uploads/2021/07/google-play-badge-1.png"
                    alt="google-play"
                  />
                </a>
                <a href="#">
                  <img
                    src="https://www.sipas.id/wp-content/uploads/2021/07/pngegg-1.png"
                    alt="google-play"
                  />
                </a>
              </div>
            </div>

            <div className=" w-full md:w-2/4">
              <StaticImage
                src="../images/homepage-sipas2.webp"
                alt="app"
                className=""
              />
            </div>
          </div>
        </section>

        <section className="bg-slate-100 pb-0 md:pb-60">
          <div className="relative mx-auto my-10 w-full max-w-screen-lg">
            <div className="flex w-full flex-col gap-4 px-5 md:w-2/4 md:px-0">
              <span className="font-semibold text-main-blue-500">
                PENGGUNA SIPAS
              </span>
              <h2 className="text-3xl font-black">
                Telah Dipercaya Berbagai Instansi
              </h2>
              <p className="w-2/3">
                Lebih dari 50 instansi di 15 kota yang tersebar di Indonesia
                telah menggunakan SIPAS.
              </p>

              <div className="mx-auto flex gap-4 md:mx-0">
                <div className="flex flex-col">
                  <span className="text-4xl font-semibold">50+</span>
                  <span>Instansi</span>
                </div>

                <div className="flex flex-col">
                  <span className="text-4xl font-semibold">15+</span>
                  <span>kota</span>
                </div>
              </div>
            </div>

            <div className="hidden md:absolute md:top-2/3 md:flex">
              <div className="flex items-center">
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/1.png"
                  alt="1"
                />
              </div>

              <div className="flex flex-col justify-center">
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/2.png"
                  alt="2"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/3-1.png"
                  alt="3"
                />
              </div>

              <div className="-mt-20 flex flex-col justify-center">
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/4.png"
                  alt="4"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/5.png"
                  alt="5"
                />
              </div>

              <div className="-mt-24 flex flex-col justify-center">
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/6.png"
                  alt="6"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/7.png"
                  alt="7"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/7.png"
                  alt="8"
                />
              </div>

              <div className="-mt-28 flex flex-col justify-center">
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/9.png"
                  alt="9"
                  width={210}
                  height={140}
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/10.png"
                  alt="10"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/14.png"
                  alt="11"
                />
              </div>

              <div className="-mt-40 flex flex-col justify-center">
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/12.png"
                  alt="12"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/13.png"
                  alt="13"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/11.png"
                  alt="14"
                />
                <img
                  src="https://www.sipas.id/wp-content/uploads/2021/08/15.png"
                  alt="15"
                />
              </div>
            </div>

            <div className="mt-10 grid grid-flow-row grid-cols-2 justify-items-center md:hidden ">
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/1.png"
                alt="1"
              />

              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/2.png"
                alt="2"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/3-1.png"
                alt="3"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/4.png"
                alt="4"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/5.png"
                alt="5"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/6.png"
                alt="6"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/7.png"
                alt="7"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/7.png"
                alt="8"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/9.png"
                alt="9"
                width={210}
                height={140}
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/10.png"
                alt="10"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/14.png"
                alt="11"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/12.png"
                alt="12"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/13.png"
                alt="13"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/11.png"
                alt="14"
              />
              <img
                src="https://www.sipas.id/wp-content/uploads/2021/08/15.png"
                alt="15"
              />
            </div>
          </div>
        </section>

        <section className="mx-auto my-10 w-full max-w-screen-lg bg-white">
          <div className="flex flex-col items-center gap-4">
            <span className="font-semibold text-main-blue-500">
              PAKET LAYANAN
            </span>
            <h2 className="text-center text-3xl font-black md:text-left">
              Dapatkan Layanan Terbaik Sesuai Kebutuhan Anda
            </h2>
            <p className="text-center md:text-left">
              Anda dapat memilih opsi berlangganan atau pembelian sekali bayar.
            </p>
          </div>

          <div className="mt-8 grid grid-cols-1 grid-rows-2 md:grid-cols-2 md:grid-rows-1">
            <div className="flex flex-col bg-main-blue-700 p-16 text-white md:rounded-l-xl">
              <span className="mb-2 text-2xl font-bold">Soho</span>
              <p className="my-4">
                Cukup berlangganan saja, anda dapat menggunakan layanan secara
                langsung tanpa kendala.
              </p>

              <div className="my-4">
                {[
                  "Tanpa Proses Instalasi, Siap Pakai",
                  "Berlangganan",
                  "24/7 Online",
                ].map((text) => (
                  <div className="flex gap-4" key={text}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="currentColor"
                      viewBox="0 0 448 512"
                      className="mr-2 inline-block h-4 w-4"
                    >
                      <path d="M438.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L160 338.7 393.4 105.4c12.5-12.5 32.8-12.5 45.3 0z" />
                    </svg>
                    <span className="font-semibold">{text}</span>
                  </div>
                ))}
              </div>
              <ConsultationButton className="mx-auto mt-auto w-fit py-3 text-white" />
            </div>

            <div className="flex flex-col bg-slate-100 p-16 text-black md:rounded-r-xl">
              <span className="mb-2 text-2xl font-bold">On Premis</span>
              <p className="my-4">
                Dapatkan kemudahan pemasangan aplikasi SIPAS di sistem Anda
                dengan sekali bayar.
              </p>

              <div className="my-4">
                {[
                  "Instalasi ke Server Anda",
                  "Bayar Sekali, Manfaat Tanpa Batas",
                  "Kontrol Penuh Data Anda",
                ].map((text) => (
                  <div className="flex gap-4" key={text}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="currentColor"
                      viewBox="0 0 448 512"
                      className="mr-2 inline-block h-4 w-4"
                    >
                      <path d="M438.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L160 338.7 393.4 105.4c12.5-12.5 32.8-12.5 45.3 0z" />
                    </svg>
                    <span className="font-semibold">{text}</span>
                  </div>
                ))}
              </div>
              <ConsultationButton className="mx-auto mt-auto w-fit py-3 text-white" />
            </div>
          </div>
          <div className="mt-10 flex justify-center">
            <a href="#" className="text-main-blue-500 underline">
              Pelajari perbedaan keduanya
            </a>
          </div>
        </section>

        <section className="mt-10 bg-slate-100">
          <div className="mx-auto mt-5 flex w-full max-w-screen-lg flex-col justify-between md:flex-row">
            <div className="flex flex-col justify-center">
              <h3 className="text-3xl font-bold">Kebutuhan Anda Unik?</h3>
              <p>
                SIPAS hadir untuk memberikan solusi sesuai dengan kebutuhan
                Anda.
              </p>

              <a
                href="#"
                className="mt-8 w-fit rounded-md border border-black p-4 px-6 font-semibold text-main-blue-500"
              >
                Hubungi Kami
              </a>
            </div>

            <div className="flex justify-center">
              <StaticImage
                src="../images/Frame.png"
                alt="frame"
                className="object-contain"
              />
            </div>
          </div>
        </section>

        <Footer />
      </div>
    </>
  );
};

export default IndexPage;

export const Head: HeadFC = () => (
  <>
    <title>SIPAS(SIstem Informasi Pengelolaan Arsip Surat)</title>
    <meta
      name="description"
      content="SIPAS adalah produk dari Sekawan Media untuk membantu dalam tata kelola persuratan dan aplikasi e-office yang dapat diakses secara online."
    ></meta>
    <link rel="canonical" href="https://www.sipas.id/"></link>
  </>
);
