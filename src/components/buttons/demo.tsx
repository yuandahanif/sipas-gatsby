import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
import React from "react";
import { twMerge } from "tailwind-merge";

interface DemoButtonProps {
  background?: "transparent" | "white";
  className?: string;
}

const DemoButton: React.FC<DemoButtonProps> = ({ background, className }) => {
  return (
    <Link
      to="#"
      className={twMerge(
        `p-5 rounded-md flex items-center border-white border-2 px-20 gap-2`,
        className
      )}
      style={{
        backgroundColor: background === "white" ? "white" : "transparent",
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor"
        viewBox="0 0 512 512"
        className="w-6 h-6"
      >
        <path d="M0 256a256 256 0 1 1 512 0A256 256 0 1 1 0 256zM188.3 147.1c-7.6 4.2-12.3 12.3-12.3 20.9V344c0 8.7 4.7 16.7 12.3 20.9s16.8 4.1 24.3-.5l144-88c7.1-4.4 11.5-12.1 11.5-20.5s-4.4-16.1-11.5-20.5l-144-88c-7.4-4.5-16.7-4.7-24.3-.5z" />
      </svg>
      <span>Demo</span>
    </Link>
  );
};

export default DemoButton;
