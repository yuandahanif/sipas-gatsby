import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
import React from "react";
import { twMerge } from "tailwind-merge";

interface ConsultationButtonProps {
  className?: string;
}

const ConsultationButton: React.FC<ConsultationButtonProps> = ({
  className,
}) => {
  return (
    <Link
      to="#"
      className={twMerge(
        `flex items-center gap-2 rounded-md bg-green-400 p-5`,
        className,
      )}
    >
      <StaticImage
        src="../../images/whatsapp-logo.svg"
        alt="whatsapp"
        className="h-6 w-6 text-white"
      />
      Konsultasi Gratis
    </Link>
  );
};

export default ConsultationButton;
