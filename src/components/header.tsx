import React, { useEffect } from "react";
import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
import ConsultationButton from "./buttons/consultation";
import DemoButton from "./buttons/demo";
import { twMerge } from "tailwind-merge";

const Header = () => {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const headerRef = React.useRef<HTMLElement>(null);
  const isHeaderVisible = React.useRef(true);

  useEffect(() => {
    const ref = headerRef.current;
    document.addEventListener("scroll", () => {
      if (ref) {
        if (window.scrollY > 100) {
          ref.classList.add("sticky");
          ref.classList.add("top-0");
          if (isHeaderVisible.current) {
            ref.animate(
              [
                { opacity: 0, transform: "translateY(-100%)" },
                { opacity: 1, transform: "translateY(0)" },
              ],
              {
                duration: 500,
                fill: "forwards",
              },
            );
          }
          isHeaderVisible.current = false;
        } else {
          ref.classList.remove("sticky");
          ref.classList.remove("top-0");
          isHeaderVisible.current = true;
        }
      }
    });
  }, []);

  return (
    <header
      ref={headerRef}
      className="sticky top-0 z-50 bg-main-blue-500 duration-500"
    >
      <div className="mx-auto flex max-w-screen-xl flex-wrap items-center justify-stretch gap-6 bg-main-blue-500 p-4  shadow-md md:shadow-none">
        <Link to="/">
          <StaticImage
            src="../images/Logo-Horizontal-Light-512x84-1.png"
            alt="logo"
            className="w-fit"
          />
        </Link>

        <div className="ml-auto text-white md:hidden">
          {!isMenuOpen && (
            <button
              className=""
              type="button"
              onClick={() => setIsMenuOpen(true)}
            >
              <svg
                viewBox="0 0 512 512"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                width="1em"
                height="1em"
                fill="currentColor"
              >
                <path d="M0 96c0-13.255 10.745-24 24-24h464c13.255 0 24 10.745 24 24s-10.745 24-24 24H24c-13.255 0-24-10.745-24-24zm0 160c0-13.255 10.745-24 24-24h464c13.255 0 24 10.745 24 24s-10.745 24-24 24H24c-13.255 0-24-10.745-24-24zm0 160c0-13.255 10.745-24 24-24h464c13.255 0 24 10.745 24 24s-10.745 24-24 24H24c-13.255 0-24-10.745-24-24z"></path>
              </svg>
            </button>
          )}

          {isMenuOpen && (
            <button
              className=""
              type="button"
              onClick={() => setIsMenuOpen(false)}
            >
              <svg
                viewBox="0 0 512 512"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                width="1em"
                height="1em"
                fill="currentColor"
              >
                <path d="M71.029 71.029c9.373-9.372 24.569-9.372 33.942 0L256 222.059l151.029-151.03c9.373-9.372 24.569-9.372 33.942 0 9.372 9.373 9.372 24.569 0 33.942L289.941 256l151.03 151.029c9.372 9.373 9.372 24.569 0 33.942-9.373 9.372-24.569 9.372-33.942 0L256 289.941l-151.029 151.03c-9.373 9.372-24.569 9.372-33.942 0-9.372-9.373-9.372-24.569 0-33.942L222.059 256 71.029 104.971c-9.372-9.373-9.372-24.569 0-33.942z"></path>
              </svg>
            </button>
          )}
        </div>

        <div
          className={twMerge(
            "w-full flex-col gap-6 text-white md:ml-auto md:flex md:w-fit md:flex-row",
            isMenuOpen ? "flex " : "hidden",
          )}
        >
          <nav className="flex flex-col items-center gap-4 md:w-full md:flex-row">
            <Link className="whitespace-nowrap text-white" to="#why">
              Mengapa SIPAS?
            </Link>
            <Link className=" whitespace-nowrap" to="#feature">
              Fitur Aplikasi
            </Link>
            <Link className=" whitespace-nowrap" to="#mobile">
              Mobile App
            </Link>
          </nav>

          <div className="z-10 mx-auto flex h-fit w-fit flex-col justify-center gap-2 md:mx-0 md:w-full md:flex-row">
            <ConsultationButton className="p-2 px-4 text-white" />
            <DemoButton className="px-4 py-2 text-black" background="white" />
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
